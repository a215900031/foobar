//
// Created by zhu on 2017/12/20.
//

#include <sstream>
#include <cstring>
#include "header/Recorder.h"

string getTime() {
    time_t nowTime;
    time(&nowTime);
    char *pz = ctime(&nowTime);
    *(pz + strlen(pz) - 1) = ' ';
    return string(pz);
}

template<typename T>
string numToString(T num) {
    stringstream sString;
    sString << num;
    return sString.str();
}

Recorder::Recorder() : _dataLine() {
}

void Recorder::record(const string head, const string message) {
    if (head == "changePassword") {
        _dataLine.push_back(getTime() + " " + head + " to " + message);
    } else {
        cerr << "wrong at record" << endl;
    }
}

void Recorder::record(const string head, ll money) {
    if (head == "withdraw") {
        _dataLine.push_back(getTime() + " " + head + " money: " + numToString(money));
    } else if (head == "save" || head == "return") {
        _dataLine.push_back(getTime() + " " + head + " money: " + numToString(money));
    } else {
        cerr << "wrong at record" << endl;
    }
}

void Recorder::record(string head, ll money, string cardAccount) {
    if (head == "shift") {
        _dataLine.push_back(
                getTime() + " " + head + " money: " + numToString(money) +
                " to account: " +
                cardAccount);
    } else if (head == "receive") {
        _dataLine.push_back(getTime() + " " + head + " money: " + numToString(money) +
                            " from account: " +
                            cardAccount);
    } else {
        cerr << "wrong at record" << endl;
    }
}

void Recorder::writeRecorder(ostream &os) {
    for (const auto &item :_dataLine) {
        os << item << endl;
    }
}


void Recorder::readRecorder(istream &is) {
    string tem;
    while (getline(is, tem)) {
        _dataLine.push_back(tem);
    }
}
