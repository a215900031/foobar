//
// Created by zhu on 2017/12/19.
//

#ifndef FOOBAR_ATM_H
#define FOOBAR_ATM_H

#include "CardControl.h"

class ATM {
public:
    /*启动ATM，运行其每个组件*/
    void run();

private:
    /*atm中所有的组件，暂且只有卡片控制系统*/
    CardControl _cardControl;

};


#endif //FOOBAR_ATM_H
