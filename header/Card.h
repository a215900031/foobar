//
// Created by zhu on 2017/12/19.
//

#ifndef FOOBAR_CARD_H
#define FOOBAR_CARD_H

#include <iostream>
#include <string>
#include <algorithm>
#include "BaseData.h"
#include "Recorder.h"

using namespace std;
using ll=long long;


class Card {
public:

    Card();

    Card(string account, string name, string ID, string password);

    virtual ~Card();

    /*获取账号名*/
    string getAccount();

    /*比较密码是否正确*/
    bool cmpPassword(string password);

    /*把卡上锁，用于输错密码三次*/
    void lockCard();

    /*判断是否被锁*/
    bool isLocked();

    /*运行，不同的卡不一样*/
    virtual void run()=0;

    /*将卡的基本数据发送到文件导航中*/
    virtual void writeBaseData(ostream &os)=0;

    virtual void readBaseData(istream &is)=0;

    /*特别数据和操作记录*/
    virtual void write()=0;

    virtual void read()=0;

    virtual string getType()=0;

    /*收到钱，即从另一张储蓄卡获得钱*/
    virtual bool recMoney(Card *from, ll money)=0;

    virtual void clearDayMoneyChange()=0;

protected:
    BaseData _baseData;

    Recorder _recorder;

    virtual void welcome()=0;

    /*存钱或者是还钱*/
    virtual void saveMoney()=0;

    /*取钱或者是借钱*/
    virtual void popMoney()=0;

    /*响应互交，输出卡片信息*/
    virtual void prtInfo();

    /*更改密码*/
    void changePassword();

    virtual void do_saveMoney(ll money)=0;

    virtual void do_popMoney(ll money)=0;

private:

};


#endif //FOOBAR_CARD_H
