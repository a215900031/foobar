//
// Created by zhu on 2017/12/20.
//

#ifndef FOOBAR_BASEDATA_H
#define FOOBAR_BASEDATA_H

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class BaseData {
public:
    BaseData();

    BaseData(string account, string name, string ID, string password);

    string getAccount();

    bool changePassword(string newPassword, string reNewPassword);

    /*把卡上锁，用于输错密码三次*/
    void lockCard();

    /*判断是否被锁*/
    bool isLocked();

    bool write(ostream &os);

    bool read(istream &is);

    bool cmpPassword(string password);

private:
    string _account;
    string _name;
    string _ID;
    string _password;
    bool _locked;
};


#endif //FOOBAR_BASEDATA_H
