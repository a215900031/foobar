//
// Created by zhu on 2017/12/20.
//

#ifndef FOOBAR_RECARDER_H
#define FOOBAR_RECARDER_H

#include <iostream>
#include <vector>
#include <ctime>
#include <cstring>
using namespace std;
using ll =long long;


class Recorder {
public:
    Recorder();

    ~Recorder() = default;

    void record(string head, string message);

    void record(string head, ll money);

    void record(string head, ll money, string cardAccount);

    void writeRecorder(ostream &os);

    void readRecorder(istream &is);

private:
    vector<string> _dataLine;
};


#endif //FOOBAR_RECARDER_H
