//
// Created by zhu on 2017/12/19.
//

#ifndef FOOBAR_CARDCONTROL_H
#define FOOBAR_CARDCONTROL_H

#include <map>
#include <fstream>
#include <iostream>
#include "Card.h"
#include "CreditCard.h"
#include "DebitCard.h"

class CardControl {
public:
    /*存储了所有注册的卡*/
    static map<string, Card *> ALLCARD;

    CardControl();

    ~CardControl();

    /*运行卡片系统*/
    bool run();

private:
    bool isNewDay();

    void getDay();

    struct {
        int year;
        int month;
        int day;
    } _lastTime;

    /*插卡*/
    bool insertCard();

    /*当前运行的卡*/
    Card *_curCard;
};


#endif //FOOBAR_CARDCONTROL_H
