//
// Created by zhu on 2017/12/19.
//

#ifndef FOOBAR_DEBITCARD_H
#define FOOBAR_DEBITCARD_H

#include "Card.h"
#include "CardControl.h"

class DebitCard : public Card {
public:
    DebitCard();

    DebitCard(string account, string name, string ID, string password, ll money,
              ll dayMaxMoneyChange = 2000, ll singMaxMoneyChange = 1000);

    ~DebitCard() override;

    void run() override;

    bool recMoney(Card *card, ll money) override;

    /*将卡的基本数据发送到文件导航中*/
    void writeBaseData(ostream &os) override;

    void clearDayMoneyChange() override;

    void readBaseData(istream &is) override;

    /*特别数据和操作记录*/
    void write() override;

    void read() override;

    string getType() override;

protected:
    ll _leftMoney;
    ll _dayMoneyChange;
    ll _singMaxMoneyChange;
    ll _dayMaxMoneyChange;

    void shiftMoney();

    void welcome() override;

    void prtInfo() override;

    void saveMoney() override;

    void popMoney() override;

    void do_saveMoney(ll money) override;

    void do_popMoney(ll money) override;
};


#endif //FOOBAR_DEBITCARD_H
