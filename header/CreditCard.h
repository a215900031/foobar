//
// Created by zhu on 2017/12/19.
//

#ifndef FOOBAR_CREDITCARD_H
#define FOOBAR_CREDITCARD_H

#include "Card.h"

class CreditCard : public Card {
public:

    CreditCard();

    CreditCard(string account, string name, string ID, string password, ll creditMoney,
               ll oweMoney,
               ll dayMaxMoneyChange, ll singMaxMoneyChange);

    ~CreditCard() override;

    void run() override;

    bool recMoney(Card *card, ll money) override;

    /*将卡的基本数据发送到文件导航中*/
    void writeBaseData(ostream &os) override;

    void readBaseData(istream &is) override;

    /*特别数据和操作记录*/
    void write() override;

    void read() override;

    string getType() override;

    void clearDayMoneyChange() override;

protected:
    ll _oweMoney;
    ll _dayMoneyChange;
    ll _creditMoney;
    ll _singMaxMoneyChange;
    ll _dayMaxMoneyChange;

    void welcome() override;

    void prtInfo() override;

    void saveMoney() override;

    void popMoney() override;

    void do_saveMoney(ll money) override;

    void do_popMoney(ll money) override;


};


#endif //FOOBAR_CREDITCARD_H
