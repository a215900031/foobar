//
// Created by zhu on 2017/12/20.
//

#include <sstream>
#include "header/BaseData.h"

using namespace std;

string getSecString(istream &is) {
    stringstream ss;
    string tem;
    std::getline(is, tem);
    ss << tem;
    ss >> tem;
    ss >> tem;
    return tem;
}

long long getSecLL(istream &is) {
    stringstream ss;
    string tem;
    long long temLL;
    std::getline(is, tem);
    ss << tem;
    ss >> tem;
    ss >> temLL;
    return temLL;
}

BaseData::BaseData(string account, string name, string ID, string password) :
        _account(account),
        _name(name),
        _ID(ID),
        _password(password),
        _locked(false) {

}

BaseData::BaseData() : BaseData("0000000000000000000", "0", "0",
                                "0") {
}

string BaseData::getAccount() {
    return _account;
}

bool BaseData::changePassword(string newPassword, string reNewPassword) {
    if (newPassword == reNewPassword) {
        _password = newPassword;
        return true;
    } else {
        cerr << "wrong in changePassword in BaseData" << endl;
        return false;
    }
}

void BaseData::lockCard() {
    if (isLocked()) {
        cerr << "it's locked" << endl;
    } else {
        _locked = true;
    }
}

bool BaseData::isLocked() {
    return _locked;
}

bool BaseData::write(ostream &os) {
    bool flag = false;
    if (os) {
        os << "Account: " << _account << endl;
        os << "Name: " << _name << endl;
        os << "ID: " << _ID << endl;
        os << "PassWord: " << _password << endl;
        os << "Locked: " << (_locked ? "True" : "False") << endl;
        flag = true;
    } else {
        cerr << "wrong at BaseData write" << endl;
    }
    return flag;
}

bool BaseData::read(istream &is) {
    if (is) {
        _account = getSecString(is);
        _name = getSecString(is);
        _ID = getSecString(is);
        _password = getSecString(is);
        _locked = getSecString(is) == "True";
    } else {
        cerr << "wrong at BaseData construct" << endl;
    }
    return true;
}

bool BaseData::cmpPassword(string password) {
    return password == _password;
}

