//
// Created by zhu on 2017/12/19.
//
#include <string>
#include <iostream>
#include <sstream>

#include "header/CardControl.h"

using namespace std;

map<string, Card *> CardControl::ALLCARD;


CardControl::CardControl() : _curCard(nullptr) {
    ifstream is("CardDir.dat");
    string tipLine;
    getline(is, tipLine);
    stringstream sStream;
    sStream << tipLine;
    sStream >> _lastTime.year;
    sStream >> _lastTime.month;
    sStream >> _lastTime.day;

    getline(is, tipLine);
    while (tipLine != "END") {
        Card *temCard = nullptr;
        if (tipLine == "CreditCard") {
            temCard = new CreditCard;
        } else if (tipLine == "DebitCard") {
            temCard = new DebitCard;
        } else {
            cerr << "wrong at construct of CardControl" << endl;
        }
        temCard->readBaseData(is);
        temCard->read();
        ALLCARD.insert(make_pair(temCard->getAccount(), temCard));
        getline(is, tipLine);
    }
    if (isNewDay()) {
        for (auto &item :CardControl::ALLCARD) {
            item.second->clearDayMoneyChange();
        }
    }
    getDay();
}

CardControl::~CardControl() {
    ofstream os("CardDir.dat");
    os << _lastTime.year << " " << _lastTime.month << " " << _lastTime.day << endl;
    for (const auto &item : CardControl::ALLCARD) {
        item.second->writeBaseData(os);
        delete item.second;
    }
    os << "END";
}

bool CardControl::run() {
    return insertCard();
}

bool CardControl::insertCard() {
    cout << "Please insert your card" << endl;
    cout << "Please input your account:(must be 19 numbers,q to quit)" << endl;
    string account;
    while (cin >> account &&
           CardControl::ALLCARD.find(account) == CardControl::ALLCARD.end()) {
        if (account == "q") {
            return false;
        }
        cout << "there is no account:" << account << endl;
        cout << "Please input a right account" << endl;
    }
    _curCard = CardControl::ALLCARD.find(account)->second;
    string password;
    int times = 0;
    if (!_curCard->isLocked()) {
        cout << "Please input your password" << endl;
        while (cin >> password && !_curCard->cmpPassword(password) && ++times < 3) {
            cout << "Password wrong!you have " << 3 - times << "times left to try"
                 << endl;
        }
        if (times == 3) {
            _curCard->lockCard();
            cout << "Lock your Card" << endl;
        } else {
            cout << "Successfully insertCard" << endl;
            _curCard->run();
        }
    } else {
        cout << "your account is Locked, Please find admin to unLock" << endl;
    }
    return true;
}


bool CardControl::isNewDay() {
    time_t nowTime;
    time(&nowTime);
    bool flag = false;
    struct tm *cur = localtime(&nowTime);
    if (cur->tm_mday > _lastTime.day || cur->tm_mon+1 > _lastTime.month ||
        cur->tm_year+1900 > _lastTime.year) {
        flag = true;
    }
    return flag;
}

void CardControl::getDay() {
    time_t nowTime;
    time(&nowTime);
    struct tm *cur = localtime(&nowTime);
    _lastTime.year = cur->tm_year+1900;
    _lastTime.month = cur->tm_mon+1;
    _lastTime.day = cur->tm_mday;
}


