#include <iostream>
#include "header/ATM.h"

using namespace std;

/*
 * CardDir只保存了三张卡的信息
 * 分别为账号：1234567891234567890  密码：均为123456
 *            1234567890123456789
 *            0123456789123456789
 * 操作记录存在账号名加.dat的文件里
*/

int main() {
    ATM atm;
    atm.run();
    return 0;
}

