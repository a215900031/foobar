//
// Created by zhu on 2017/12/19.
//
#include <sstream>
#include "../header/CreditCard.h"

long long getSecLL(istream &is);

CreditCard::CreditCard() : CreditCard("0", "0", "0",
                                      "0", 2500, 0, 1000, 1000) {

}

CreditCard::CreditCard(string account, string name, string ID, string password,
                       ll creditMoney, ll oweMoney, ll dayMaxMoneyChange,
                       ll singMaxMoneyChange) : Card(account,
                                                     name, ID,
                                                     password), _creditMoney(creditMoney),
                                                _oweMoney(oweMoney),
                                                _dayMoneyChange(0),
                                                _singMaxMoneyChange(singMaxMoneyChange),
                                                _dayMaxMoneyChange(dayMaxMoneyChange) {

}


void CreditCard::run() {
    welcome();
    int input = -1;
    cin >> input;
    while (input != 0) {
        switch (input) {
            case 1:
                prtInfo();
                break;
            case 2:
                popMoney();
                break;
            case 3:
                saveMoney();
                break;
            case 4:
                changePassword();
                break;
            case 5:
                welcome();
                break;
            default:
                cout << "Please input a number between 0-6" << endl;
                break;
        }
        cin >> input;
    }
}

void CreditCard::welcome() {
    cout << "****************************" << endl;
    cout << "* 1. get information of the card" << endl;
    cout << "* 2. pop money" << endl;
    cout << "* 3. ret money" << endl;
    cout << "* 4. change the password" << endl;
    cout << "* 5. show tips again" << endl;
    cout << "* 0. exit" << endl;
    cout << "****************************" << endl;
}

void CreditCard::prtInfo() {
    Card::prtInfo();
    cout << "creditMoney:" << _creditMoney << endl;
    cout << "oweMoney:" << _oweMoney << endl;
    cout << "dayMoneyChange:" << _dayMoneyChange << endl;
    cout << "singMaxMoneyChange:" << _singMaxMoneyChange << endl;
    cout << "dayMaxMoneyChange:" << _dayMaxMoneyChange << endl;
}

void CreditCard::saveMoney() {
    cout << "***************" << endl;
    cout << "now your owe money:" << _oweMoney << endl;
    cout << "how many money you want to return(Pleas less than owe money)" << endl;
    ll money = 0;
    while (cin >> money && money > _oweMoney) {
        cout << "input again, less than " << _oweMoney << endl;
    }
    do_saveMoney(money);
    cout << "now you oweMoney is:" << _oweMoney << endl;
    cout << "******************" << endl;
    _recorder.record("return", money);
}

void CreditCard::do_saveMoney(ll money) {
    _oweMoney -= money;
    _dayMoneyChange += money;
}

void CreditCard::popMoney() {
    cout << "********************" << endl;
    cout << "now your owe money is:" << _oweMoney << endl;
    cout << "you can pop money today is:" <<
         min(_dayMaxMoneyChange + _dayMoneyChange,
             min(_singMaxMoneyChange, _creditMoney - _oweMoney)) << endl;
    cout << "how many money you want pop:";
    ll money = 0;
    while (cin >> money &&
           (money <= 0 || money % 100 != 0 ||
            money > _dayMaxMoneyChange + _dayMoneyChange ||
            money > _singMaxMoneyChange)) {
        cout << "Please make sure money is n times of 100 and greater than 0" << endl;
        cout << "and be less than "
             << min(_creditMoney - _oweMoney,
                    min(_singMaxMoneyChange, _dayMaxMoneyChange + _dayMoneyChange))
             << endl;
        cout << "Please input again" << endl;
    };
    do_popMoney(money);
    _recorder.record("withdraw", money);
    cout << "now your oweMoney : " << _oweMoney << endl;
    cout << "***************************" << endl;
}

void CreditCard::do_popMoney(ll money) {
    _oweMoney += money;
    _dayMoneyChange -= money;
}

bool CreditCard::recMoney(Card *card, ll money) {
    if (money <= _oweMoney) {
        do_saveMoney(money);
        _recorder.record("receive", money, card->getAccount());
        return true;
    } else {
        return false;
    }
}


CreditCard::~CreditCard() {
    write();
}

void CreditCard::writeBaseData(ostream &os) {
    os << getType() << endl;
    _baseData.write(os);
    os << "creditMoney: " << _creditMoney << endl;
    os << "oweMoney: " << _oweMoney << endl;
}

void CreditCard::readBaseData(istream &is) {
    _baseData.read(is);
    _creditMoney = getSecLL(is);
    _oweMoney = getSecLL(is);
}

void CreditCard::write() {
    ofstream os((getAccount() + ".dat").c_str());

    os << "oweMoney: " << _oweMoney << endl;
    os << "creditMoney: " << _creditMoney << endl;
    os << "dayMoneyChange: " << _dayMoneyChange << endl;
    os << "singMaxMoneyChange: " << _singMaxMoneyChange << endl;
    os << "dayMaxMoneyChange: " << _dayMaxMoneyChange << endl;
    _recorder.writeRecorder(os);
}

void CreditCard::read() {
    ifstream is((getAccount() + ".dat").c_str());
    _oweMoney = getSecLL(is);
    _creditMoney = getSecLL(is);
    _dayMoneyChange = getSecLL(is);
    _singMaxMoneyChange = getSecLL(is);
    _dayMaxMoneyChange = getSecLL(is);
    _recorder.readRecorder(is);
}

string CreditCard::getType() {
    return string("CreditCard");
}

void CreditCard::clearDayMoneyChange() {
    _dayMoneyChange = 0;
}
