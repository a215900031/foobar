//
// Created by zhu on 2017/12/19.
//

#include "../header/Card.h"

Card::Card(string account, string name, string ID, string password) : _baseData(account,
                                                                                name, ID,
                                                                                password) {

}

Card::Card() : Card("0", "0", "0", "0") {
}

string Card::getAccount() {
    return _baseData.getAccount();
}

void Card::changePassword() {
    cout << "************************" << endl;
    string newPassword, reNewPassword;
    cout << "Please input new password(password must be 6 numbers)" << endl;
    cin >> newPassword;
    cout << "Please input new password again" << endl;
    cin >> reNewPassword;
    while (newPassword != reNewPassword || newPassword.size() != 6) {
        if (newPassword != reNewPassword) {
            cout << "your twice input is different, please input twice again" << endl;
        } else if (newPassword.size() != 6) {
            cout << "password must be 6 numbers" << endl;
        }
        cout << "Please input new password" << endl;
        cin >> newPassword;
        cout << "Please input new password again" << endl;
        cin >> reNewPassword;
    }
    _recorder.record("changePassword", newPassword);
    _baseData.changePassword(newPassword, reNewPassword);
    cout << "change password to:" << newPassword << endl;
}

bool Card::cmpPassword(string password) {
    return _baseData.cmpPassword(password);
}

bool Card::isLocked() {
    return _baseData.isLocked();
}

void Card::lockCard() {
    _baseData.lockCard();
}


void Card::prtInfo() {
    _baseData.write(cout);
}

Card::~Card() =
default;



