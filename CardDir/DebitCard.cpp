//
// Created by zhu on 2017/12/19.
//

#include <sstream>
#include "../header/DebitCard.h"

long long getSecLL(istream &is);

DebitCard::DebitCard(string account, string name, string ID, string password, ll money,
                     ll dayMaxMoneyChange, const ll singMaxMoneyChange) : Card(account,
                                                                               name, ID,
                                                                               password),
                                                                          _leftMoney(
                                                                                  money),
                                                                          _dayMoneyChange(
                                                                                  0),
                                                                          _singMaxMoneyChange(
                                                                                  singMaxMoneyChange),
                                                                          _dayMaxMoneyChange(
                                                                                  dayMaxMoneyChange) {


}

DebitCard::DebitCard() : DebitCard("0", "0", "0",
                                   "0", 10000, 0, 1000) {

}

DebitCard::~DebitCard() {
    write();
}

void DebitCard::welcome() {
    cout << "****************************" << endl;
    cout << "* 1. get information of the card" << endl;
    cout << "* 2. pop money" << endl;
    cout << "* 3. save money" << endl;
    cout << "* 4. change the password" << endl;
    cout << "* 5. shift money to other" << endl;
    cout << "* 6. show tips again" << endl;
    cout << "* 0. exit" << endl;
    cout << "****************************" << endl;
}

void DebitCard::run() {
    welcome();
    int input = -1;
    cin >> input;
    while (input != 0) {
        switch (input) {
            case 1:
                prtInfo();
                break;
            case 2:
                popMoney();
                break;
            case 3:
                saveMoney();
                break;
            case 4:
                changePassword();
                break;
            case 5:
                shiftMoney();
                break;
            case 6:
                welcome();
                break;
            default:
                cout << "Please input a number between 0-6" << endl;
                break;
        }
        cin >> input;
    }
}

void DebitCard::prtInfo() {
    Card::prtInfo();
    cout << "leftMoney:" << _leftMoney << endl;
    cout << "dayMoneyChange:" << _dayMoneyChange << endl;
    cout << "singMaxMoneyChange:" << _singMaxMoneyChange << endl;
}

void DebitCard::saveMoney() {
    cout << "***************" << endl;
    cout << "how many money you want to save" << endl;
    ll money = 0;
    cin >> money;
    do_saveMoney(money);
    _recorder.record("save", money);
    cout << "save " << "money:" << money << endl;
    cout << "now you money is:" << _leftMoney << endl;

}

void DebitCard::popMoney() {
    cout << "********************" << endl;
    cout << "now your money is:" << _leftMoney << endl;
    cout << "you can pop money:"
         << min(_leftMoney,
                min(_dayMaxMoneyChange + _dayMoneyChange, _singMaxMoneyChange)) << endl;
    cout << "how many money you want pop:";
    ll money = 0;
    while (cin >> money &&
           (money <= 0 || money % 100 != 0 ||
            money > _dayMaxMoneyChange + _dayMoneyChange ||
            money > _singMaxMoneyChange) || money > _leftMoney) {
        cout << "Please make sure money is n times of 100 and greater than 0" << endl;
        cout << "and be less than "
             << min(_leftMoney,
                    min(_singMaxMoneyChange, _dayMaxMoneyChange + _dayMoneyChange))
             << endl;
        cout << "Please input again" << endl;
    };
    do_popMoney(money);
    _recorder.record("withdraw", money);
    cout << "withdraw money:" << money << endl;
    cout << "now your leftMoney : " << _leftMoney << endl;
    cout << "***************************" << endl;
}


void DebitCard::shiftMoney() {
    cout << "********************" << endl;
    cout << "now your money is:" << _leftMoney << endl;
    cout << "you can shift Max money today is:" << _dayMaxMoneyChange + _dayMoneyChange
         << endl;
    cout << "how many money you want shift:";
    ll money = 0;
    while (cin >> money &&
           (money <= 0 || money > _dayMaxMoneyChange + _dayMoneyChange ||
            money > _singMaxMoneyChange)) {
        cout << "Please make sure money is greater than 0" << endl;
        cout << "and be less than "
             << min(_leftMoney,
                    min(_singMaxMoneyChange, _dayMaxMoneyChange + _dayMoneyChange))
             << endl;
        cout << "Please input again" << endl;
    };
    string name, reName;
    cout << "whose card you want to give(please input account)" << endl;
    cin >> name;
    cout << "Please input again" << endl;
    cin >> reName;
    while (name != reName) {
        cout << "wrong!input again(q to quit)" << endl;
        cout << "whose card you want to give(please input account)" << endl;
        cin >> name;
        if (name == "q") {
            break;
        }
        cout << "Please input again" << endl;
        cin >> reName;
    }
    if (name == reName) {
        if (CardControl::ALLCARD.find(name)->second->recMoney(this, money)) {
            do_popMoney(money);
            cout << "sent money " << money << " to Account:" << getAccount() << endl;
            cout << "now your leftMoney : " << _leftMoney << endl;
            _recorder.record("shift", money, name);
        } else {
            cout << "you shift too much money, but he didn't owe that much money" << endl;
        }
        cout << "***************************" << endl;
    }
}

void DebitCard::do_saveMoney(ll money) {
    _leftMoney += money;
    _dayMoneyChange += money;
}

void DebitCard::do_popMoney(ll money) {
    _leftMoney -= money;
    _dayMoneyChange -= money;
}


bool DebitCard::recMoney(Card *card, ll money) {
    do_saveMoney(money);
    _recorder.record("receive", money, card->getAccount());
    return true;
}

void DebitCard::writeBaseData(ostream &os) {
    os << getType() << endl;
    _baseData.write(os);
    os << "leftMoney: " << _leftMoney << endl;
}

void DebitCard::readBaseData(istream &is) {
    _baseData.read(is);
    _leftMoney = getSecLL(is);
}

void DebitCard::write() {
    ofstream os((getAccount() + ".dat").c_str());
    os << "leftMoney: " << _leftMoney << endl;
    os << "dayMoneyChange: " << _dayMoneyChange << endl;
    os << "singMaxMoneyChange: " << _singMaxMoneyChange << endl;
    os << "dayMaxMoneyChange: " << _dayMaxMoneyChange << endl;
    _recorder.writeRecorder(os);
}

void DebitCard::read() {
    ifstream is((getAccount() + ".dat").c_str());
    _leftMoney = getSecLL(is);
    _dayMoneyChange = getSecLL(is);
    _singMaxMoneyChange = getSecLL(is);
    _dayMaxMoneyChange = getSecLL(is);
    _recorder.readRecorder(is);
}

string DebitCard::getType() {
    return std::string("DebitCard");
}

void DebitCard::clearDayMoneyChange() {
    _dayMoneyChange = 0;
}


















